package eu.bhsystems.sample.project.business.systemcore;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by BrunoHorta on 19/05/15.
 */
public class DateUtils {
    public static String getFormattedDate(Date d, TimeZone t) {
        if (d == null || t == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy  HH:mm");
        isoFormat.setTimeZone(t);
        return isoFormat.format(d);
    }

    public static String getOnlyFormattedDate(Date d, TimeZone t) {
        if (d == null || t == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
        isoFormat.setTimeZone(t);
        return isoFormat.format(d);
    }

    public static String getFormattedDate(Date d) {
        if (d == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy  HH:mm");

        return isoFormat.format(d);
    }

    public static String getOnlyFormattedDate(Date d) {
        if (d == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
        return isoFormat.format(d);
    }

    public static String getOnlyHours(Date d) {
        if (d == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("HH:mm");
        return isoFormat.format(d);
    }

    public static Date dateFromString(String date) throws ParseException {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return format.parse(date);
    }
}
