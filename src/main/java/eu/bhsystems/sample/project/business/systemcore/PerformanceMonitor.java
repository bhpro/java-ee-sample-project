package eu.bhsystems.sample.project.business.systemcore;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 * Created by bruno on 17/04/15.
 */
public class PerformanceMonitor {

    @AroundInvoke
    public Object measure(InvocationContext invocationContext) throws Exception {
        long start = System.currentTimeMillis();
        try {
            return invocationContext.proceed();
        } finally {
          //  System.out.println(invocationContext.getMethod().getDeclaringClass() + " : " + invocationContext.getMethod().getName() + " executed in " + (System.currentTimeMillis() - start) + " milliseconds");
        }

    }
}
