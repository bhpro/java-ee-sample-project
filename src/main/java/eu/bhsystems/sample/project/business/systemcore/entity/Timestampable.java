package eu.bhsystems.sample.project.business.systemcore.entity;

import java.util.Date;

/**
 * Created by BrunoHorta on 21/03/15.
 */
public interface Timestampable {
    Date getCreated();

    Date getLastModified();
}
