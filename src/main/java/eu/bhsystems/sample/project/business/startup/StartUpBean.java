package eu.bhsystems.sample.project.business.startup;


import eu.bhsystems.sample.project.business.security.bondary.RoleRepository;
import eu.bhsystems.sample.project.business.security.entity.RoleType;
import eu.bhsystems.sample.project.business.systemcore.SystemConstants;
import eu.bhsystems.sample.project.business.users.UsersRepository;
import org.slf4j.Logger;
import eu.bhsystems.sample.project.business.security.entity.Role;
import eu.bhsystems.sample.project.business.systemcore.NoRegistryFoundException;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.users.UserType;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.io.File;


/**
 * Created by BrunoHorta on 23/03/15.
 */
@Singleton
@Startup
public class StartUpBean {

    @Inject
    Logger logger;
    @Inject
    private UsersRepository usersRepository;
    @Inject
    private RoleRepository roleRepository;

    @PostConstruct
    public void init() {
        createBaseFolders();
        createBasicRoles();
        createSuperAdmin();

    }




    private void createSuperAdmin() {
        if (usersRepository.count() == 0) {
            User user = new User("Administrator", "xptoxpto", "admin@sample.eu");
            user.setUserType(UserType.SYSTEM);
            try {
                user.addRole(roleRepository.findByType(RoleType.SUPERADMIN));
                user.addRole(roleRepository.findByType(RoleType.ADMINISTRATOR));
            } catch (NoRegistryFoundException e) {
                logger.error(e.getMessage());
            }
            usersRepository.store(user);
        }

    }

    private void createBasicRoles() {
        if (roleRepository.count() == 0) {
            roleRepository.store(new Role(RoleType.ADMINISTRATOR));
            roleRepository.store(new Role(RoleType.SUPERADMIN));
        }

    }

    private void createBaseFolders() {
        File appDir = new File(SystemConstants.APP_DIRECTORY_PATH);
        if (!appDir.exists()) {
            appDir.mkdirs();
        }

    }
}
