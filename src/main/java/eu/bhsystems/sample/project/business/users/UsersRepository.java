package eu.bhsystems.sample.project.business.users;

import eu.bhsystems.sample.project.business.security.CustomRealm;
import eu.bhsystems.sample.project.business.security.bondary.RoleRepository;
import eu.bhsystems.sample.project.business.security.entity.RoleType;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.RealmSecurityManager;
import eu.bhsystems.sample.project.business.security.UsernameNotFoundException;
import eu.bhsystems.sample.project.business.security.entity.Role;
import eu.bhsystems.sample.project.business.systemcore.NoRegistryFoundException;
import eu.bhsystems.sample.project.business.systemcore.bondary.PersistenceRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


/**
 * Created by BrunoHorta on 21/03/15.
 */
@Stateless
public class UsersRepository extends PersistenceRepository<User> {
    @Inject
    private Event<User> eventSrc;

    @Inject
    @NewUserCreate
    private Event<User> eventNewUser;
    @Inject
    @ResetUserPassword
    private Event<User> eventResetPassword;
    @EJB
    private RoleRepository roleRepository;

    public UsersRepository() {
        super(User.class);
    }


    @Override
    public User store(User entity) {
        if (entity.getId() == 0)
            eventNewUser.fire(entity);
        User user = super.store(entity);
        eventSrc.fire(user);
        return user;
    }

    @Override
    public List<User> findAllOrderedByDefaultField() {
        return findAllOrderedByName();
    }


    public List<User> getLoggedUsers() throws UsernameNotFoundException {
        List<User> users = new ArrayList<>();
        CustomRealm customRealm = (CustomRealm) ((RealmSecurityManager) SecurityUtils.getSecurityManager()).getRealms().toArray()[0];
        for (String email : customRealm.getLoggedUsers()) {
            users.add(findByUsername(email));
        }
        return users;
    }

    public List<String> getLoggedEmailUsers() throws UsernameNotFoundException {

        CustomRealm customRealm = (CustomRealm) ((RealmSecurityManager) SecurityUtils.getSecurityManager()).getRealms().toArray()[0];
        return customRealm.getLoggedUsers();
    }

    public void resetPassword(User entity) {
        if (entity.getId() == 0)
            return;
        eventResetPassword.fire(entity);

    }

    /**
     * Return all User in database, Ordered by Name
     *
     * @return List
     */
    public List<User> findAllOrderedByName() {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        criteria.select(member).where(cb.notEqual(member.get("userType"), UserType.SYSTEM)).orderBy(cb.asc(member.get("name")));
        return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getResultList();
    }

    /**
     * Return all User in database, Ordered by Name
     *
     * @return List
     */
    public List<User> findAllOrderedByRole(RoleType roleType) {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<User> criteria = cb.createQuery(User.class);
        Root<User> member = criteria.from(User.class);
        Expression<Collection<Role>> roles = member.get("roles");
        try {
            criteria.select(member).where(cb.equal(member.get("active"), true), cb.isMember(roleRepository.findByType(roleType), roles), cb.notEqual(member.get("userType"), UserType.SYSTEM)).orderBy(cb.asc(member.get("name")));
        } catch (NoRegistryFoundException e) {
            return new ArrayList<>();
        }
        return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getResultList();
    }

    /**
     * Find User by username,
     * Not accept null parameter
     *
     * @param username
     * @return User
     */
    public User findByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username, "Username must be specified");
        try {
            CriteriaBuilder cb = getManager().getCriteriaBuilder();
            CriteriaQuery<User> criteria = cb.createQuery(User.class);
            Root<User> member = criteria.from(User.class);
            criteria.select(member).where(cb.equal(member.get("email"), username));
            return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getSingleResult();
        } catch (NoResultException ex) {
            throw new UsernameNotFoundException();
        }
    }

    public boolean isUsernameExists(User user) {
        Objects.requireNonNull(user, "User must be specified");
        try {
            return findByUsername(user.getEmail()).getId() != user.getId();
        } catch (UsernameNotFoundException e) {
            return false;
        }
    }

    public User findByName(String name) throws UsernameNotFoundException {
        try {
            CriteriaBuilder cb = getManager().getCriteriaBuilder();
            CriteriaQuery<User> criteria = cb.createQuery(User.class);
            Root<User> member = criteria.from(User.class);
            criteria.select(member).where(cb.equal(member.get("name"), name));
            return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getSingleResult();
        } catch (NoResultException ex) {
            throw new UsernameNotFoundException();
        }
    }

    public User findByEmail(String email) throws UsernameNotFoundException {
        try {
            CriteriaBuilder cb = getManager().getCriteriaBuilder();
            CriteriaQuery<User> criteria = cb.createQuery(User.class);
            Root<User> member = criteria.from(User.class);
            criteria.select(member).where(cb.equal(member.get("email"), email));
            return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getSingleResult();
        } catch (NoResultException ex) {
            throw new UsernameNotFoundException();
        }
    }

    public void updatePassword(User user, String newPassword) {
        User user1 = get(user.getId());
        user1.setPassword(newPassword);
    }

    public User stampLogin(User user) {
        return get(user.getId()).stampLogin();
    }
}
