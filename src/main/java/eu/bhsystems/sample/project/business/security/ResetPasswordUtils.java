package eu.bhsystems.sample.project.business.security;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;

/**
 * Created by BrunoHorta on 25/04/15.
 */
public class ResetPasswordUtils {

    public static String createHashForSetPassword(String username) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Objects.requireNonNull(username, "Username must be specified");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        String message = "RESETPASSMYCLINITEC"
                + username;
        return "h=" + URLEncoder.encode(rot13(username) + "|" + ToHexString(md.digest(message.getBytes())), "UTF-8");
    }

    public static String ToHexString(byte[] bytes) {
        int i;
        StringBuilder sb = new StringBuilder();
        for (i = 0; i < bytes.length; i++) {
            sb.append(String.format("%02x", bytes[i]));
        }
        return sb.toString();
    }


    public static String isResetPasswordTokenValid(String token) throws UnsupportedEncodingException, InvalidTokenException, NoSuchAlgorithmException {
        Objects.requireNonNull(token, "Token must be specified");
        int tokenDecoded = URLDecoder.decode(token, "UTF-8").lastIndexOf("|");
        if (tokenDecoded < 0) {
            throw new InvalidTokenException();
        }
        String username = rot13(token.substring(0, tokenDecoded));
        String checkSum = token.substring(tokenDecoded + 1);
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        String message = "RESETPASSMYCLINITEC" + username;
        if (!checkSum.equals(ToHexString(md.digest(message.getBytes())))) {
            throw new InvalidTokenException();
        }
        return username;


    }

    public static String rot13(final String s) {
        StringBuilder sb = new StringBuilder(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 'a' && c <= 'm') {
                c += 13;
            } else if (c >= 'A' && c <= 'M') {
                c += 13;
            } else if (c >= 'n' && c <= 'z') {
                c -= 13;
            } else if (c >= 'N' && c <= 'Z') {
                c -= 13;
            }
            sb.append(c);
        }
        return sb.toString();
    }

}
