package eu.bhsystems.sample.project.business.security;

import javax.ejb.ApplicationException;

/**
 * Created by bruno on 15/01/15.
 */
@ApplicationException(rollback = false)
public class LoginErrorException extends Exception {
    public LoginErrorException() {
        super("Login Error");
    }
}


