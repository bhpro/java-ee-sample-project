package eu.bhsystems.sample.project.business.systemcore.bondary;

import eu.bhsystems.sample.project.business.systemcore.entity.Identifiable;

/**
 * Created by BrunoHorta on 21/03/15.
 */
public interface Repository<T extends Identifiable> {
    Class<T> getType();

    T store(T entity);

    T get(long id);

    T getEager(long id, String... fields);

    void remove(T entity);

    long count();


}
