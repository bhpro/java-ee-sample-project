package eu.bhsystems.sample.project.business.security;

import javax.ejb.ApplicationException;

/**
 * Created by bruno on 15/01/15.
 */
@ApplicationException(rollback = false)
public class InvalidTokenException extends Exception {
    public InvalidTokenException() {
        super("Invalid Token ");
    }
}


