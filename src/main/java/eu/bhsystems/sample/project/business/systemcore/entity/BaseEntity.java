package eu.bhsystems.sample.project.business.systemcore.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by BrunoHorta on 21/03/15.
 */
@MappedSuperclass
public abstract class BaseEntity implements Identifiable, Timestampable, Serializable {


    public BaseEntity() {
        this.created = new Date();
    }

    @Version
    private int version;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;


    protected boolean active = Boolean.TRUE;

    protected boolean editable = Boolean.TRUE;
    protected boolean deleted;

    @Override
    public long getId() {
        return this.id;
    }

    @Override
    public Date getCreated() {
        return this.created;
    }

    @Override
    public Date getLastModified() {
        return this.updated;
    }

    @PrePersist
    protected void hashItNew() {
        this.created = new Date();
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @PreUpdate
    protected void hashIt() {
        this.updated = new Date();

    }


    public String getFormattedDate(Date d) {
        if (d == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy  HH:mm");

        return isoFormat.format(d);
    }

    public String getFormattedDate(Date d, String pattern) {
        if (d == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat(pattern);

        return isoFormat.format(d);
    }

    public static String getOnlyFormattedDate(Date d) {
        if (d == null) {
            return null;
        }
        SimpleDateFormat isoFormat = new SimpleDateFormat("dd-MM-yyyy");
        return isoFormat.format(d);
    }

    public String getDateCreatedFormatted() {
        return getFormattedDate(created);
    }

    public boolean isNew() {
        return this.id == 0;
    }


    public abstract boolean isEditable();

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getCreatedHour() {
        return getFormattedDate(getCreated(), "HH:mm");
    }

    public String getUpdatedHour() {
        return getFormattedDate(getLastModified(), "HH:mm");
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        if (version != that.version) return false;
        return id == that.id;

    }

    @Override
    public int hashCode() {
        int result = version;
        result = 31 * result + (int) (id ^ (id >>> 32));
        return result;
    }
}
