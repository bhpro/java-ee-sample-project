package eu.bhsystems.sample.project.business.security;

import javax.ejb.ApplicationException;

/**
 * Created by bruno on 15/01/15.
 */
@ApplicationException(rollback = false)
public class UsernameNotFoundException extends Exception {
    public UsernameNotFoundException() {
        super("Request username does not exists in the database");
    }
}


