package eu.bhsystems.sample.project.business.systemcore.bondary;


import eu.bhsystems.sample.project.business.systemcore.PerformanceMonitor;
import eu.bhsystems.sample.project.business.systemcore.entity.Identifiable;
import eu.bhsystems.sample.project.business.systemcore.NoRegistryFoundException;

import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by BrunoHorta on 21/03/15.
 */
@Interceptors(PerformanceMonitor.class)
public abstract class PersistenceRepository<T extends Identifiable> implements Repository<T> {
    protected static final String HIBERNATE_CACHE = "org.hibernate.cacheable";
    public static String OWNER_COMPANY_FIELD = "ownerCompany";
    @PersistenceContext
    private EntityManager manager;

    private Class<T> type;


    public PersistenceRepository() {
    }

    public PersistenceRepository(Class<T> type) {
        this.type = type;
    }

    @Override
    public Class<T> getType() {
        return this.type;
    }

    @Override
    public T store(T entity) {
        T merged = merge(entity);
        manager.persist(merged);
        return merged;
    }

    @Override
    public T getEager(long id, String... fields) {
        CriteriaBuilder criteriaBuilder = getManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        for (String relation : fields) {
            root.fetch(relation, JoinType.LEFT);
        }
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id"), id));
        return getManager().createQuery(criteriaQuery).setHint(HIBERNATE_CACHE, true).getSingleResult();
    }

    protected List<T> getAll() {
        CriteriaBuilder criteriaBuilder = getManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        criteriaQuery.from(type);
        return getManager().createQuery(criteriaQuery).setHint(HIBERNATE_CACHE, true).getResultList();
    }

    protected List<T> getAllEager(String orderByAttribute, String... fields) {
        CriteriaBuilder criteriaBuilder = getManager().getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        for (String relation : fields) {
            root.fetch(relation, JoinType.LEFT);
        }
        criteriaQuery.select(root).distinct(true).orderBy(criteriaBuilder.asc(root.get(orderByAttribute)));
        return getManager().createQuery(criteriaQuery).setHint(HIBERNATE_CACHE, true).getResultList();
    }

    @Override
    public T get(long id) {
        return manager.find(type, id);
    }


    @Override
    public void remove(T entity) {
        entity = merge(entity);
        manager.remove(entity);
    }


    private T merge(T entity) {
        return manager.merge(entity);
    }

    protected EntityManager getManager() {
        return manager;
    }


    /**
     * Return all Products in database, Ordered by Parameter
     *
     * @return List
     */
    protected List<T> findAllOrderedBy(String atrribute, boolean asc) {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<T> criteria = cb.createQuery(getType());
        Root<T> member = criteria.from(getType());
        criteria.select(member).orderBy(asc ? cb.asc(member.get(atrribute)) : cb.desc(member.get(atrribute)));
        return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getResultList();
    }


    protected T findByField(String field, Object value) throws NoRegistryFoundException {
        try {
            CriteriaBuilder cb = getManager().getCriteriaBuilder();
            CriteriaQuery<T> criteria = cb.createQuery(getType());
            Root<T> registry = criteria.from(getType());
            criteria.select(registry).distinct(true).where(cb.equal(registry.get(field), value));
            return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getSingleResult();
        } catch (Exception ex) {
            throw new NoRegistryFoundException();
        }
    }

    protected T findByField(String field, Object value, long ownerCompanyId) throws NoRegistryFoundException {
        try {
            CriteriaBuilder cb = getManager().getCriteriaBuilder();
            CriteriaQuery<T> criteria = cb.createQuery(getType());
            Root<T> registry = criteria.from(getType());
            criteria.select(registry).distinct(true).where(cb.equal(registry.get(field), value), cb.equal(registry.get(OWNER_COMPANY_FIELD), ownerCompanyId));
            return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getSingleResult();
        } catch (Exception ex) {
            throw new NoRegistryFoundException();
        }
    }

    protected List<T> findAllByField(String field, Object value) {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<T> criteria = cb.createQuery(getType());
        Root<T> registry = criteria.from(getType());
        criteria.select(registry).distinct(true).where(cb.equal(registry.get(field), value));
        return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getResultList();
    }

    protected List<T> findAllByField(String field, Object value, String atrribute, boolean asc) {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<T> criteria = cb.createQuery(getType());
        Root<T> registry = criteria.from(getType());
        criteria.select(registry).distinct(true).where(cb.equal(registry.get(field), value)).orderBy(asc ? cb.asc(registry.get(atrribute)) : cb.desc(registry.get(atrribute)));
        return getManager().createQuery(criteria.distinct(true)).setHint(HIBERNATE_CACHE, true).getResultList();
    }

    @Override
    public long count() {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<Long> criteria = cb.createQuery(Long.class);
        criteria.select(cb.countDistinct(criteria.from(getType())));
        return getManager().createQuery(criteria).setHint(HIBERNATE_CACHE, true).getSingleResult();
    }


    public abstract List<T> findAllOrderedByDefaultField();

}
