package eu.bhsystems.sample.project.business.systemcore.entity;

/**
 * Created by BrunoHorta on 21/03/15.
 */
public interface Identifiable {
    long getId();
}
