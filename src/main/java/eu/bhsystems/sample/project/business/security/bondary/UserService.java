package eu.bhsystems.sample.project.business.security.bondary;

import eu.bhsystems.sample.project.business.users.UsersRepository;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.security.LoginErrorException;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Created by BrunoHorta on 03/04/15.
 */
@Stateless
public class UserService {

    @EJB
    private UsersRepository usersRepository;

    public User login(String username, String password) throws LoginErrorException {
        try {
            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(username, password);
            token.setRememberMe(true);
            subject.login(token);
            User user;
            user = usersRepository.findByUsername(username);
            if (!user.isActive()) {
                throw new LoginErrorException();
            }
            user.stampLogin();
            usersRepository.store(user);
            Session session = subject.getSession();
            session.setAttribute("currentUser", user);
            token.clear();
            return user;
        } catch (Exception ex) {
            throw new LoginErrorException();
        }

    }


}