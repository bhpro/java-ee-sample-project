package eu.bhsystems.sample.project.business.systemcore;

import org.apache.commons.io.FilenameUtils;

import java.text.Normalizer;
import java.util.UUID;

/**
 * Created by brunohorta on 21/11/15.
 */
public class SystemConstants {


    public static String ROOT_FOLDER = "/v01/SAMPLE";
    public static String IMAGES_FOLDER = ROOT_FOLDER + "/images/";
    public static String IMAGES_SYSTEM_FOLDER = "/system/";
    public static String USERS_PHOTO_FOLDER = "photousers/";
    public static String APP_DIRECTORY_PATH = ROOT_FOLDER + "/app/";

    public static String makeSecureFileName(String name) {
        if (name == null)
            return "";

        String extension = FilenameUtils.getExtension(name);
        if (!extension.trim().isEmpty()) {
            name = name.substring(0, name.length() - (extension.length() + 1));
        }
        name = Normalizer.normalize(name, Normalizer.Form.NFD);
        name = name.replaceAll("[^\\p{ASCII}]", "");
        String trim = name.replace("\'", "").replace("\"", "")
                .replace("\\", "")
                .replace("/", "_").replace(",", "")
                .replace(":", "_")
                .replace(";", "_")
                .replace(".", "_")
                .replace("+", "").replace("*", "_")
                .replace("&", "_")
                .replace("?", "").replace("#", "_").trim();
        if (!extension.contains(".") && !extension.trim().isEmpty()) {
            extension = "." + extension;
        }
        String filename = trim + extension;
        if (filename.trim().isEmpty())
            filename = UUID.randomUUID().toString().replace("-", "");
        return filename.toLowerCase();
    }

}
