package eu.bhsystems.sample.project.business.security.bondary;

import eu.bhsystems.sample.project.business.security.entity.RoleType;
import eu.bhsystems.sample.project.business.security.entity.Role;
import eu.bhsystems.sample.project.business.systemcore.NoRegistryFoundException;
import eu.bhsystems.sample.project.business.systemcore.bondary.PersistenceRepository;

import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by BrunoHorta on 21/04/15.
 */
@Stateless
public class RoleRepository extends PersistenceRepository<Role> {
    public RoleRepository() {
        super(Role.class);
    }

    @Override
    public List<Role> findAllOrderedByDefaultField() {
        return findAllOrderedByType();
    }


    public List<Role> findAllOrderedByType() {
        CriteriaBuilder cb = getManager().getCriteriaBuilder();
        CriteriaQuery<Role> criteria = cb.createQuery(getType());
        Root<Role> member = criteria.from(getType());
        criteria.select(member).where(cb.notEqual(member.get("type"), RoleType.SUPERADMIN)).orderBy(cb.asc(member.get("type")));
        return getManager().createQuery(criteria).getResultList();
    }

    public Role findByType(RoleType roleType) throws NoRegistryFoundException {
        return findByField("type", roleType);
    }
}
