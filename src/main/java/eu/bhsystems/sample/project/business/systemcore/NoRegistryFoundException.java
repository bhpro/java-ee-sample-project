package eu.bhsystems.sample.project.business.systemcore;

import javax.ejb.ApplicationException;

/**
 * Created by bruno on 15/01/15.
 */
@ApplicationException(rollback = false)
public class NoRegistryFoundException extends Exception {
    public NoRegistryFoundException() {
        super("Request object does not exists in the database");
    }
}


