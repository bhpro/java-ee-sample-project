package eu.bhsystems.sample.project.business.systemcore.entity;

import eu.bhsystems.sample.project.business.users.User;

/**
 * Created by BrunoHorta on 21/03/15.
 */
public interface Auditable {
    User getCreatedBy();
    User getLastUpDateBy();
}
