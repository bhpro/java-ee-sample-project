package eu.bhsystems.sample.project.business.users;


import eu.bhsystems.sample.project.business.security.entity.Role;
import eu.bhsystems.sample.project.business.security.entity.RoleType;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.util.SimpleByteSource;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import eu.bhsystems.sample.project.business.systemcore.entity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

/**
 * Created by BrunoHorta on 21/03/15.
 */

@Entity
public class User extends BaseEntity {

    private static final long serialVersionUID = 7717775051222290515L;
    //Instance Members
    @NotNull(message = "{name.empty}")
    @NotBlank(message = "{name.empty}")
    @Size(min = 2, max = 100, message = "{name.size}")
    protected String name;

    @Email
    protected String email;



    @NotNull(message = "{password.empty}")
    @NotBlank(message = "{password.empty}")
    @Size(min = 4, max = 200, message = "{password.size}")
    private String password;

    @NotBlank
    @Size(min = 5, max = 400)
    private String salt;

    private int loginCount;


    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;

    @NotNull(message = "{userType.null}")
    @Enumerated(EnumType.STRING)
    private UserType userType = UserType.APPLICATION;

    @NotEmpty(message = "{role.empty}")
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Role> roles;

    private String photoFileName;

    @Transient
    private boolean online;

    public User() {
    }

    public User(String name, String password, String email) {
        this.name = name;
        this.email = email;
        Objects.requireNonNull(email, "Email  must be specified");
        Objects.requireNonNull(password, "Password must be specified");
        setPassword(password);
    }

    //Accessord / Mutators

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }




    @Override
    public boolean isEditable() {
        return true;
    }

    public String getPassword() {
        return password;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public User setLastLogin(Date lastLogin) {
        Objects.requireNonNull(lastLogin, "Date for Last Login must be specified");
        this.lastLogin = lastLogin;
        return this;
    }

    public void setPassword(String password) {
        Objects.requireNonNull(password, "Password must be specified");
        RandomNumberGenerator rng = new SecureRandomNumberGenerator();
        this.salt = new SimpleByteSource(rng.nextBytes() + email).toHex();
        this.password = new Sha512Hash(password, this.salt).toHex();


    }

    public Set<Role> getRoles() {
        return Collections.unmodifiableSet(roles);
    }


    public String getSalt() {
        return salt;
    }


    public int getLoginCount() {
        return loginCount;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public User stampLogin() {
        this.loginCount++;
        this.lastLogin = new Date();
        return this;
    }

    public void addRole(Role role) {
        Objects.requireNonNull(role, "Role must be specified");
        if (this.roles == null) {
            this.roles = new HashSet<>();
        }
        this.roles.add(role);
    }

    public void removeRole(Role role) {
        Objects.requireNonNull(role, "Role must be specified");
        Objects.requireNonNull(roles, "Roles must initialised");
        this.roles.remove(role);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return Objects.equals(loginCount, user.loginCount) &&

                Objects.equals(password, user.password) &&
                Objects.equals(salt, user.salt) &&
                Objects.equals(email, user.email) &&
                Objects.equals(lastLogin, user.lastLogin) &&
                Objects.equals(roles, user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), password, salt, loginCount, email, lastLogin, roles);
    }

    public boolean hasRole(RoleType type) {
        for (Role role : roles) {
            if (role.getType().equals(type))
                return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }



    public boolean hasPhoto() {
        return photoFileName != null;
    }

    public String nameAbreviature() {
        if (isNew() || (getName() == null || getName().trim().isEmpty()))
            return "NU";
        int i = getName().split(" ").length;
        return (getName().split(" ")[0].substring(0, 1) +getName().split(" ")[i - 1].substring(0, 1)).toUpperCase();
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getPhotoFileName() {
        return photoFileName;
    }

    public void setPhotoFileName(String photoFileName) {
        this.photoFileName = photoFileName;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public boolean isOnline() {
        return online;
    }

    public void setName(String name) {
        this.name = name;
    }



    public void setEmail(String email) {
        this.email = email;
    }



    public boolean isSuperAdmin() {
        if (roles == null)
            return false;
        for (Role role : roles) {
            if (role.getType().equals(RoleType.SUPERADMIN))
                return true;
        }

        return false;
    }

    public boolean isAdmin() {
        if (roles == null)
            return false;
        for (Role role : roles) {
            if (role.getType().equals(RoleType.ADMINISTRATOR))
                return true;
        }

        return false;
    }
}
