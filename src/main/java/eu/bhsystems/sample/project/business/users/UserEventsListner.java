package eu.bhsystems.sample.project.business.users;

import org.slf4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by BrunoHorta on 26/04/15.
 */
@RequestScoped
public class UserEventsListner {

    @Inject
    Logger logger;



    public void newUserCreate(@Observes(during = TransactionPhase.AFTER_SUCCESS) @NewUserCreate User user) {

    }

    public void resetPassword(@Observes(during = TransactionPhase.AFTER_SUCCESS) @ResetUserPassword User user) {

    }

    private String getBaseUrl() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        StringBuffer link = request.getRequestURL();
        String protocol = link.toString().split("/")[0];
        String dnsip = link.toString().split("/")[2];
        return protocol + "//" + dnsip;
    }
}
