package eu.bhsystems.sample.project.business.security.entity;


import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.systemcore.entity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

/**
 * @author brunohorta
 */

@Entity
public class Role extends BaseEntity {

    @Basic(optional = false)
    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false)
    private RoleType type;

    @ManyToMany(mappedBy = "roles")
    private List<User> users;

    public Role() {
    }

    public Role(RoleType type) {
        this.type = type;
    }

    @Override
    public boolean isEditable() {
        return true;
    }
    public void setType(RoleType type) {
        this.type = type;
    }

    public RoleType getType() {
        return type;
    }

    public List<User> getUsers() {
        return Collections.unmodifiableList(users);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return type == role.type;

    }

    @Override
    public int hashCode() {
        return type != null ? type.hashCode() : 0;
    }
}

