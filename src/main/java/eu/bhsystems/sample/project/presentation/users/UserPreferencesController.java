package eu.bhsystems.sample.project.presentation.users;


import eu.bhsystems.sample.project.presentation.systemcore.BaseController;

import javax.faces.bean.SessionScoped;
import javax.inject.Named;

/**
 * Created by BrunoHorta on 03/04/15.
 */
@Named
@SessionScoped
public class UserPreferencesController extends BaseController {

}
