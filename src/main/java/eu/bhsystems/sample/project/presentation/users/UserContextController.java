package eu.bhsystems.sample.project.presentation.users;


import eu.bhsystems.sample.project.business.users.UsersRepository;
import org.apache.shiro.SecurityUtils;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.security.UsernameNotFoundException;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.HashMap;


/**
 * Created by BrunoHorta on 23/03/15.
 */
public abstract class UserContextController implements Serializable {


    @EJB
    private UsersRepository usersRepository;
    private HashMap<String, User> loggedUser = new HashMap<>();


    public User getLoggedUser() throws UsernameNotFoundException {

            loggedUser.put(SecurityUtils.getSubject().getPrincipal().toString(), usersRepository.findByUsername(SecurityUtils.getSubject().getPrincipal().toString()));
        return this.loggedUser.get(SecurityUtils.getSubject().getPrincipal().toString());

    }


}
