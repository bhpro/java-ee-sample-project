package eu.bhsystems.sample.project.presentation.users;


import eu.bhsystems.sample.project.business.security.bondary.RoleRepository;
import eu.bhsystems.sample.project.business.security.entity.Role;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;


/**
 * Created by BrunoHorta on 23/03/15.
 */
@ViewScoped
@Named
public class RolesListProduces implements Serializable {

    @Inject
    private RoleRepository roleRepository;


    private List<Role> roles;


    public List<Role> getRoles() {
        return this.roles;
    }

    @PostConstruct
    public void init() {
        roles = roleRepository.findAllOrderedByType();
    }
}
