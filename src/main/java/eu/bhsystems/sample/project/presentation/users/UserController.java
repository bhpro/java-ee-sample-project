package eu.bhsystems.sample.project.presentation.users;


import eu.bhsystems.sample.project.presentation.util.PasswordGenerator;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.CroppedImage;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.users.UsersRepository;
import eu.bhsystems.sample.project.business.security.entity.Role;
import eu.bhsystems.sample.project.business.systemcore.SystemConstants;
import eu.bhsystems.sample.project.presentation.systemcore.BaseController;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.faces.view.ViewScoped;
import javax.imageio.stream.FileImageOutputStream;
import javax.inject.Named;
import javax.servlet.ServletContext;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BrunoHorta on 21/03/15.
 */
@Named
@ViewScoped
public class UserController extends BaseController {

    @EJB
    private UsersRepository usersRepository;
    private CroppedImage croppedImage;

    private String newImageName;
    @Produces
    @Named
    private User userSelected;

    private List<Role> tempRoles;

    boolean savePhoto;

    @PostConstruct
    public void initNewMember() {
        if (hasPID()) {
            userSelected = usersRepository.get(Long.parseLong(getPID()));
            tempRoles = new ArrayList<>(userSelected.getRoles());
            savePhoto = userSelected.getPhotoFileName() != null;
        } else {
            userSelected = new User();

        }
    }

    public void register() throws Exception {

        if (usersRepository.isUsernameExists(userSelected)) {
            showWarningMessage(getString("username_duplicate"), getString("username_duplicate"));
            return;
        }
        if (!savePhoto && userSelected.isNew())
            userSelected.setPhotoFileName(null);
        else if (!savePhoto) {
            userSelected.setPhotoFileName(null);
        }

        if (userSelected.getId() == 0)
            userSelected.setPassword(PasswordGenerator.generateRandomPassword());

        if (checkAndShowErrorMessages(userSelected)) return;
        usersRepository.store(userSelected);
        redirectToIndex();

    }

    public void resetPassword() {

        if (checkAndShowErrorMessages(userSelected)) return;
        usersRepository.resetPassword(userSelected);
        redirectToIndex();

    }
    public void onRoleSelected(ValueChangeEvent valueChangeEvent) {
        ArrayList<Role> oldValue = (ArrayList<Role>) valueChangeEvent.getOldValue();
        ArrayList<Role> newValue = (ArrayList<Role>) valueChangeEvent.getNewValue();
        if (oldValue != null)
            for (Object o : oldValue) {
                if (o != null) {
                    Role role = (Role) o;
                    userSelected.removeRole(role);
                }
            }
        if (newValue != null)
            for (Object o : newValue) {
                if (o != null) {
                    Role role = (Role) o;
                    userSelected.addRole(role);
                }
            }
    }



    public List<Role> getTempRoles() {
        return tempRoles;
    }

}
