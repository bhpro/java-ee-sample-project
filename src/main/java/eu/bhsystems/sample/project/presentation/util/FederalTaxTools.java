package eu.bhsystems.sample.project.presentation.util;

/**
 * Created by BrunoHorta on 26/04/15.
 */
public class FederalTaxTools {

    public static boolean IsValidNIF(String nif) {
        Integer checkDigit;
        String characterReg = "[0-9]{9}";
        if (nif.matches(characterReg)) {
            checkDigit = nif.charAt(0) * 9;
            for (int i = 2; i <= 8; i++) {
                checkDigit += nif.charAt(i - 1) * (10 - i);
            }

            checkDigit = 11 - (checkDigit % 11);

            if (checkDigit >= 10)
                checkDigit = 0;

            if (checkDigit.toString().equals(nif.charAt(8) + ""))
                return true;
        }

        return false;
    }
}
