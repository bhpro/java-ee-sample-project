package eu.bhsystems.sample.project.presentation.systemcore;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.slf4j.Logger;
import eu.bhsystems.sample.project.business.users.UsersRepository;
import eu.bhsystems.sample.project.business.security.UsernameNotFoundException;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

/**
 * Created by BrunoHorta on 03/04/15.
 */
@Named
@RequestScoped
public class LoginController extends BaseController {
    @Inject
    private UsersRepository usersRepository;
    @Inject
    Logger logger;
    private String username;
    private String password;
    private boolean remember;

    public void submit() throws IOException {
        try {
            UsernamePasswordToken token = new UsernamePasswordToken(username, password, Boolean.FALSE);
            SecurityUtils.getSubject().login(token);
            usersRepository.store(usersRepository.findByUsername(username).stampLogin());
            facesContext.getExternalContext().getSessionMap().put("user", SecurityUtils.getSubject());
            redirectToDashboard();

        } catch (AuthenticationException e) {
            facesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, getString("login_error"), null));
            logger.warn("LOGIN FAIL - " + username);

        } catch (UsernameNotFoundException e) {
            redirect404();
        }
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRemember() {
        return remember;
    }

    public void setRemember(boolean remember) {
        this.remember = remember;
    }
}
