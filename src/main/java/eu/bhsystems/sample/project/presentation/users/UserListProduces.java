package eu.bhsystems.sample.project.presentation.users;


import eu.bhsystems.sample.project.presentation.systemcore.GenericListProducer;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.users.UsersRepository;

import javax.faces.view.ViewScoped;
import javax.inject.Named;


/**
 * Created by BrunoHorta on 23/03/15.
 */
@Named
@ViewScoped
public class UserListProduces extends GenericListProducer<User, UsersRepository> {

}
