package eu.bhsystems.sample.project.presentation.systemcore;

import eu.bhsystems.sample.project.business.systemcore.bondary.PersistenceRepository;
import eu.bhsystems.sample.project.business.systemcore.entity.BaseEntity;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by BrunoHorta on 23/03/15.
 */

public abstract class GenericListProducer<ENTITY extends BaseEntity, REPOSITORY extends PersistenceRepository<ENTITY>> implements Serializable {
    @EJB
    private REPOSITORY repository;
    @Inject
    protected FacesContext facesContext;

    private List<ENTITY> list = new ArrayList<>();
    private List<ENTITY> listFilter;
    private ENTITY selected;

    @PostConstruct
    public void retrieveAllOrderedByDefaultField() {
        list = repository.findAllOrderedByDefaultField();
    }

    public List<ENTITY> getList() {
        return list;
    }

    public void setList(List<ENTITY> list) {
        this.list = list;
    }

    public List<ENTITY> getListFilter() {
        return listFilter;
    }

    public void setListFilter(List<ENTITY> listFilter) {
        this.listFilter = listFilter;
    }

    public ENTITY getSelected() {
        return selected;
    }

    public void setSelected(ENTITY selected) {
        this.selected = selected;
    }

    public void openDetail() {
        try {
            facesContext.getExternalContext().redirect("edit.xhtml?pid=" + selected.getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
