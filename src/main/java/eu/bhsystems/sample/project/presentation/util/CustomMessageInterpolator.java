package eu.bhsystems.sample.project.presentation.util;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;

import java.util.Locale;

/**
 * Created by BrunoHorta on 24/03/15.
 */
public class CustomMessageInterpolator extends ResourceBundleMessageInterpolator {

    private Locale locale;

    @Override
    public String interpolate(String s, Context context) {
        return super.interpolate(s, context, locale);
    }

    @Override
    public String interpolate(String s, Context context, Locale locale) {
        return super.interpolate(s, context, locale);
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
