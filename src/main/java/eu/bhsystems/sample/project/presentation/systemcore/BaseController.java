package eu.bhsystems.sample.project.presentation.systemcore;

import eu.bhsystems.sample.project.presentation.util.CustomMessageInterpolator;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.users.UsersRepository;
import eu.bhsystems.sample.project.business.security.UsernameNotFoundException;
import eu.bhsystems.sample.project.business.systemcore.DateUtils;
import eu.bhsystems.sample.project.business.systemcore.entity.BaseEntity;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.*;
import java.util.*;

/**
 * Created by BrunoHorta on 03/04/15.
 */
public class BaseController<T extends BaseEntity> implements Serializable {

    @Inject
    protected FacesContext facesContext;
    @Inject
    protected Validator validator;
    protected Locale locale;
    protected User loggedUser;
    protected TimeZone timeZone;
    private Session session;
    private int activeIndexTab;
    @Inject
    protected Logger logger;
    @EJB
    private UsersRepository usersRepository;

    public BaseController() {
        timeZone = Calendar.getInstance().getTimeZone();
    }

    private void fillLoggedUser() {
        try {
            this.loggedUser = usersRepository.findByUsername(SecurityUtils.getSubject().getPrincipal().toString());
        } catch (UsernameNotFoundException e) {
            redirectAccessDenied();
        }
    }

    protected Session getSession() {
        if (session == null) {
            session = SecurityUtils.getSubject().getSession();
        }
        return session;
    }

    public void saveCookie(String cookieName, Object value) {
        Cookie c = new Cookie(cookieName, value.toString());
        c.setMaxAge(Integer.MAX_VALUE);
        String p = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        if (p == null || p.isEmpty()) {
            p = "/app";
        }
        c.setPath(p);
        HttpServletResponse resp = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        resp.addCookie(c);

    }

    public Object getCookieValue(String cookieName) {
        HttpServletRequest x = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        Cookie[] c = x.getCookies();
        String cookieTab = "0";
        if (c != null && c.length > 0) {
            for (Cookie cookie : c) {
                if (cookie.getName().equals(cookieName)) {
                    cookieTab = cookie.getValue();
                    break;
                }
            }
        }
        return Integer.parseInt(cookieTab);
    }

    public void saveValueOnSession(String key, Object value) {
        getSession().setAttribute(key, value);

    }

    public Object getValueFromSession(String key) {
        return getSession().getAttribute(key);
    }

    public void handleTabChange(int i) {
        activeIndexTab = i;
    }

    public int getActiveIndexTab() {
        return activeIndexTab;
    }



    public Locale getLocale() {
        return locale = facesContext.getViewRoot().getLocale();
    }

    protected void addMessage(FacesMessage message) {
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void redirectAccessDenied() {
        try {
            facesContext.getExternalContext().redirect("/access.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void execute(String... scripts) {
        for (String script : scripts) {
            RequestContext.getCurrentInstance().execute(script);
        }
    }

    public void redirect404() {
        try {
            facesContext.getExternalContext().redirect("/404.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void redirect404(String reason) {
        try {
            facesContext.getExternalContext().addResponseHeader("reason",reason);
            facesContext.getExternalContext().redirect("/404.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public User getLoggedUser() {
        if (loggedUser == null)
            fillLoggedUser();
        return loggedUser;
    }

    protected String getString(String value) {
        return ResourceBundle.getBundle("i18n", facesContext.getViewRoot().getLocale()).getString(value);
    }

    public void redirectToIndex() {
        try {
            facesContext.getExternalContext().redirect("index.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void redirectTo(String target) {
        try {
            facesContext.getExternalContext().redirect(target);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void redirectToDashboard() {
        try {
            facesContext.getExternalContext().redirect("/app/dashboard.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void redirectToLogin() {
        try {
            facesContext.getExternalContext().redirect("login.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void showWarningMessage(String summary, String detail) {
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_WARN, summary, detail);
        facesContext.addMessage(null, m);
    }

    protected void showInfoMessage(String summary, String detail) {
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail);
        facesContext.addMessage(null, m);


    }

    public void update(String... fields) {
        for (String field : fields) {
            RequestContext.getCurrentInstance().update(field);
        }
    }

    protected void showErrorMessage(String summary, String detail) {
        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
        facesContext.addMessage(null, m);

    }

    protected Set<ConstraintViolation<T>> validate(T entity) {
        CustomMessageInterpolator customMessageInterpolator = new CustomMessageInterpolator();
        customMessageInterpolator.setLocale(facesContext.getViewRoot().getLocale());
        ValidatorFactory validatorFactory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(customMessageInterpolator)
                .buildValidatorFactory();
        validator = validatorFactory.getValidator();
        return validator.validate(entity);

    }

    /**
     * Return PID if Exists is not then return NULL
     */
    protected String getPID() {
        Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();
        return params.get("pid");
    }

    protected Long getPIDConvertedInLong() {

        return Long.parseLong(getPID());
    }

    /**
     * Return Param if Exists is not then return NULL
     */
    protected String getParam(String param) {
        Map<String, String> params = facesContext.getExternalContext().getRequestParameterMap();
        return params.get(param);
    }

    /**
     * Check if PID Exists
     */
    protected boolean hasPID() {
        return getPID() != null;
    }

    /**
     * Check Constraints if exists show message
     *
     * @param entity extends Identifiable
     * @return true if exists violations
     */
    protected boolean checkAndShowErrorMessages(T entity) {
        Set<ConstraintViolation<T>> constraintViolations = validate(entity);
        if (constraintViolations.isEmpty())
            return false;

        for (ConstraintViolation<T> violation : constraintViolations) {
            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, violation.getMessage(), "");
            facesContext.addMessage(null, m);
        }
        return true;

    }

    public String getFormatedDate(Date date) {
        return DateUtils.getFormattedDate(date, timeZone);
    }

    public String getOnlyFormatedDate(Date date) {
        return DateUtils.getOnlyFormattedDate(date, timeZone);
    }

    public String getOnlyHours(Date date) {
        return DateUtils.getOnlyHours(date);
    }

    private String getBaseUrl() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        StringBuffer link = request.getRequestURL();
        String protocol = link.toString().split("/")[0];
        String dnsip = link.toString().split("/")[2];
        return protocol + "//" + dnsip;
    }
}
