package eu.bhsystems.sample.project.presentation.users;

import eu.bhsystems.sample.project.business.security.ResetPasswordUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import eu.bhsystems.sample.project.business.users.User;
import eu.bhsystems.sample.project.business.users.UsersRepository;
import eu.bhsystems.sample.project.business.security.InvalidTokenException;
import eu.bhsystems.sample.project.business.security.UsernameNotFoundException;
import eu.bhsystems.sample.project.presentation.systemcore.BaseController;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * Created by BrunoHorta on 25/04/15.
 */
@Model
@ViewScoped
public class PasswordResetController extends BaseController<User> {

    @Inject
    Logger logger;
    private String resetPassword;
    private String resetConfirmPassword;
    private String token;
    private String username;
    private User user;

    @Inject
    private UsersRepository usersRepository;

    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        token = params.get("h");
        if (token == null) {
            redirectToLogin();
            logger.error("no query string define");
        }

        try {
            username = ResetPasswordUtils.isResetPasswordTokenValid(token);
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException | InvalidTokenException e) {
            logger.error(e.getMessage());
            redirectToLogin();
        }
        try {
            user = usersRepository.findByUsername(username);
        } catch (UsernameNotFoundException e) {
            logger.error(e.getMessage());
            redirectToLogin();
        }
    }

    public String getResetConfirmPassword() {
        return resetConfirmPassword;
    }

    public void setResetConfirmPassword(String resetConfirmPassword) {
        this.resetConfirmPassword = resetConfirmPassword;
    }

    public String getResetPassword() {
        return resetPassword;
    }

    public void setResetPassword(String resetPassword) {
        this.resetPassword = resetPassword;
    }

    public void reset() {
        if (!resetPassword.equals(resetConfirmPassword))
            showErrorMessage(getString("password_not_match"), getString("password_not_match"));
        usersRepository.updatePassword(user, resetPassword);
        login();

    }

    private void login() {
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, resetPassword);
        token.setRememberMe(true);
        subject.login(token);
        User user = usersRepository.stampLogin(this.user);
        Session session = subject.getSession();
        session.setAttribute("currentUser", user);
        token.clear();
        redirectToDashboard();


    }
}
